package es.gpsdatacollector;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class File {
    // Source: https://stackoverflow.com/questions/14376807/how-to-read-write-string-from-a-file-in-android

    private static final String fileName = "gpsES_cache.txt";
    private static Activity act = null;

    public static void clearFile(Context context){
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write("");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
        updateScrollView("");
    }

    public static boolean isEmpty(Context context){
        if(readFile(context).compareTo("")==0) return true;
        else return false;
    }

    public static void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_APPEND));

            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
        updateScrollView(File.readFile(context));
    }

    public static String readFile(Context context) {
        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                boolean one = true;
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    if(one){
                        stringBuilder.append(receiveString);
                    }else{
                        stringBuilder.append("\n"+receiveString);
                    }
                    one = false;
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            //Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            //Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static void updateScrollView(final String text){
        act.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                TextView sv = (TextView) act.findViewById(R.id.scrollView);
                sv.setText(text);

                Matcher m = Pattern.compile("\r\n|\r|\n").matcher(text);
                int lines = 1;
                while (m.find())
                {
                    lines ++;
                }

                TextView dataCount = (TextView) act.findViewById(R.id.dataCount);
                String positions = Integer.toString(lines);
                dataCount.setText(positions+act.getResources().getString(R.string.fileCount));
            }
        });
    }

    public static void setActivity(Activity activity){
        act = activity;
    }

}
