package es.gpsdatacollector;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.OnSuccessListener;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class LocationThread extends Thread{
    private Context context;
    private Activity activity;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient locProvider;
    private TextView txtLocation;
    private ActivityRecognitionClient arclient;
    private PendingIntent pIntent;

    private String position = "";
    private String act = "activity:\"Unknown\",conf:\"100\"";

    public LocationThread(Context c, Activity a, LocationRequest req, LocationCallback callback, FusedLocationProviderClient cl, TextView t, ActivityRecognitionClient arc, PendingIntent p){
        context = c;
        activity = a;
        locationRequest = req;
        locationCallback = callback;
        locProvider = cl;
        txtLocation = t;
        arclient = arc;
        pIntent = p;
    }

    @Override
    public void run(){
        int delay = 0; // delay for 0 sec.
        int period = 2000; // repeat every 2 sec.
        Timer timer = new Timer();

        IntentFilter filter = new IntentFilter();
        filter.addAction("personal.gpsdatacollector.ACTIVITY_RECOGNITION_DATA");
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                act =  String.format("activity:\"%s\",conf:\"%s\"",intent.getStringExtra("Activity"),intent.getExtras().getInt("Confidence"));
            }
        }, filter);
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }
}
