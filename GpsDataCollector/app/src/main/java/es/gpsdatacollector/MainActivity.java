package es.gpsdatacollector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.FusedLocationProviderClient;

public class MainActivity extends AppCompatActivity {

    FusedLocationProviderClient locProvider;
    Context ct;
    Activity act;
    private TextView txtLocation;

    private ActivityRecognitionClient arclient;
    PendingIntent pIntent;

    private LocationThread locThread;
    private LocationManager locManager;
    private LocationListener locListener;

    private String activ = "\"activity\":\"Unknown\",\"conf\":\"100\"";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ct = this.getApplicationContext();
        act = this;
        File.setActivity(this);

        TextView sv = (TextView) findViewById(R.id.scrollView);
        sv.setMovementMethod(new ScrollingMovementMethod());
        File.updateScrollView(File.readFile(ct));

        txtLocation = (TextView)findViewById(R.id.textBox);


        // Set button link
        Button linkButton = (Button)findViewById(R.id.buttonLink);
        linkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://es.ricardochaves.pt/"+LocationThread.getMacAddr()));
                startActivity(browserIntent);
            }
        });

        // Check GPS ON
        LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.gpsAlert))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            builder.create().show();
        }

            //arclient = ActivityRecognition.getClient(ct);
        //Intent intent = new Intent(this, ActivityRecognitionService.class);
        //pIntent = PendingIntent.getService(this, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locListener = new LocationListener(){
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    double lat = location.getLatitude();
                    double lon = location.getLongitude();
                    long timestamp = location.getTime();
                    String addr = LocationThread.getMacAddr();
                    String position = String.format("\"addr\":\"%s\",\"time\":\"%d\",\"lat\":\"%f\",\"lon\":\"%f\",\"accuracy\":\"%f\"",addr,timestamp,lat,lon,accuracy);
                    String data = String.format("{%s,%s}",position,activ);
                    //ServerConnection.sendGET(position,context);
                    ServerConnection.sendPOST(data,getApplicationContext());
                    //Log.d("GPSpos","listener:got position: "+data);
                    txtLocation.setText(data);
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {}

            @Override
            public void onProviderEnabled(String s) {}

            @Override
            public void onProviderDisabled(String s) {}
        };
        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    1000 );
        }
        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(this, new String[] {  android.Manifest.permission.ACCESS_FINE_LOCATION  },
                    1001 );
        }
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, locListener);
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 5, locListener);

        Button clearCache = (Button)findViewById(R.id.buttonClearFile);
        clearCache.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(getResources().getString(R.string.deleteCacheTitle))
                        .setMessage(getResources().getString(R.string.deleteCacheConfirmation))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Toast.makeText(MainActivity.this, getResources().getString(R.string.deleteCacheSuccess), Toast.LENGTH_SHORT).show();
                                File.clearFile(MainActivity.this);
                            }})
                        .setNegativeButton(getResources().getString(R.string.no), null).show();
            }
        });

        Button sendData = (Button)findViewById(R.id.buttonSend);
        sendData.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ServerConnection.sendCache(MainActivity.this);
                File.updateScrollView(File.readFile(MainActivity.this));
            }
        });

        Button copyData = (Button)findViewById(R.id.buttonCopy);
        copyData.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                TextView sv = (TextView) findViewById(R.id.scrollView);
                String text = sv.getText().toString();
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copying", text);
                clipboard.setPrimaryClip(clip);

                Toast toast = Toast.makeText(MainActivity.this, getResources().getString(R.string.copied), Toast.LENGTH_SHORT);
            }
        });

        //bootThread();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //bootThread();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //bootThread();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //bootThread();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //bootThread();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //bootThread();
    }

    /*
    private void bootThread(){
        if(locThread == null){
            locThread = new LocationThread(getApplicationContext(),MainActivity.this,locationRequest,locationCallback,locProvider,txtLocation,arclient,pIntent);;
            locThread.start();
            Log.d("startThread()","New one: Starting thread...");
        }else if(locThread.isInterrupted()){
            locThread.start();
            Log.d("startThread()","Interrupted: Starting thread...");
        }
    }
    */

}
