package es.gpsdatacollector;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServerConnection {

    public static void sendPOST(String data, final Context context){
        Log.d("sendPOST","call. Sending: "+data);
        final String s = data+"\n";

        Thread t = new Thread(new Runnable(){
            @Override
            public void run(){
                //buffer.add(s);
                File.writeToFile(s,context);
                //Log.d("Network","Testing wifi...");
                if(!isConnectedWifi(context)) return;
                //Log.d("sendPOST","hasWifi");
                //Log.d("Network","Testing internet...");
                if( !isOnline() ) return;
                //Log.d("sendPOST","isOnline");
                //Log.d("Network","Connection available.");
                try {
                    String dataToSend = File.readFile(context);
                    //File.clearFile(context);
                    //Log.d("dataToSend", dataToSend);

                    OkHttpClient client = new OkHttpClient();
                    RequestBody formBody = new FormBody.Builder()
                            .add("data", dataToSend)
                            .build();
                    Request request = new Request.Builder()
                            .url(context.getResources().getString(R.string.serverAddr))
                            .post(formBody)
                            .build();

                    Response response= client.newCall(request).execute();
                    if(response.isSuccessful()){
                        //Log.d("ResponseBodySuccess","success!");
                        File.clearFile(context);
                    }
                    response.close();
                }catch(Exception e){
                    //Log.d("sendPOST_exception",Log.getStackTraceString(e));
                }
            }
        });
        t.start();
    }

    public static void sendCache(Context c){
        final Context context = c;
        Thread t = new Thread(new Runnable(){
            @Override
            public void run(){
                if( !isOnline() ) return;
                //Log.d("sendPOST","isOnline");
                //Log.d("Network","Connection available.");
                try {
                    String dataToSend = File.readFile(context);

                    OkHttpClient client = new OkHttpClient();
                    RequestBody formBody = new FormBody.Builder()
                            .add("data", dataToSend)
                            .build();
                    Request request = new Request.Builder()
                            .url(context.getResources().getString(R.string.serverAddr))
                            .post(formBody)
                            .build();

                    Response response= client.newCall(request).execute();
                    if(response.isSuccessful()){
                        //Log.d("ResponseBodySuccess","success!");
                        File.clearFile(context);
                    }
                    response.close();
                }catch(Exception e){
                    //Log.d("sendPOST_exception",Log.getStackTraceString(e));
                }
            }
        });
        t.start();
    }

    // 2 different methods to test internet connection.
    // Test through InetAddress class
    private static boolean isInternetAvailable(){
        try {
            InetAddress ipAddr = InetAddress.getByName("www.ua.pt");
            int timeout = 5000; //ms
            return ipAddr.isReachable(timeout);
        } catch (Exception e) {
            //Log.d("NetworkException",Log.getStackTraceString(e));
            return false;
        }
    }

    // Test through DNS
    public static boolean isOnline(){
        try {
            int timeoutMs = 1500;
            Socket sock = new Socket();
            //SocketAddress sockaddr = new InetSocketAddress("46.101.42.250", 80);
            SocketAddress sockaddr = new InetSocketAddress("46.101.42.250",1935);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (Exception e) {
            //Log.d("NetworkException",Log.getStackTraceString(e));
            return false;
        }
    }

    // Source: https://stackoverflow.com/questions/13007707/detect-if-connection-is-wifi-3g-or-edge-in-android @deepu-t
    private static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public static boolean isConnectedWifi(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public static boolean isConnectedMobile(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public static boolean isConnectedFast(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && isConnectionFast(info.getType(),info.getSubtype()));
    }

    private static boolean isConnectionFast(int type, int subType){
        if(type==ConnectivityManager.TYPE_WIFI){
            return true;
        }else if(type==ConnectivityManager.TYPE_MOBILE){
            switch(subType){
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps
                /*
                 * Above API level 7, make sure to set android:targetSdkVersion
                 * to appropriate level to use these
                 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        }else{
            return false;
        }
    }
}
