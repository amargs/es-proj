# Architecture

![Esquema_arquitetura](/assets/images/architecture.jpg)

## Data gathering
The fundamental information format for this project is GPS data. In order to collect realistic and valid GPS information, an Android App was developed and published to the [Play Store](https://play.google.com/store/apps/details?id=es.gpsdatacollector).

At the time of writing the App had a small userbase distributed over 3 continents.

![App_world](/assets/images/app_world.png)

The above mentioned application acts as a "sensor", periodically sending information to a Cloud Server, which provides a HTTP API for anyone to access. Information can be accessed using 3 different query types:

* Querying the full user file:

	`/[MAC_ADDR_OF_USER]`

* Querying a specific user's positions starting from timestamp T:

	`/api/[MAC_ADDR_OF_USER]?s=T`

* Querying every user's positions starting from timestamp T. This type of query returns only positions of any user that are more recent than T. Sorted by timestamp.

	`/api?s=T`

## Processing data
An entry point starts by periodically attempting to fetch new data from the Cloud Server, posting it to Kafka Topics. The Kafka Broker handles and provides access to the data.

The received information is then organized into our information model, and fed to the database for persistency. At this point in the architecture the relevant information about the POIs (Points Of Interest) is created, stored in the database and sent to a new Kafka Topic.

The database handles the persistency of the data and provides a reliable source of information for the Spring application to use.

## Application and API
The Spring Boot application retrieves the required information from the database, logs any relevant output to Kafka and provides a REST API to its clients. The API requests are being constantly stored in cache in order to improve the system's performance.

### REST API
The REST API is a simple endpoint able to serve multiple clients, multiple user interfaces while also allowing clients to register new Points of Interest.

Information wise, the following data is exposed through this REST API:

* Users' locations and routes
* Points of Interest
* Users passing through a certain POI

## User Interfaces
The system supports multiple User Interfaces in any technology. All the developers have to do is connect to the system's REST API.

A simple example UI was developed and is deployed ready to be used.
