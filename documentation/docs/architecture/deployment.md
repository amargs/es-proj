The overall development, build, testing and deployment of the system crosses multiple machines and technologies:

## Git
Development of every component in the system (Android app, cloud server, Spring application, ...) is done using Git, providing a reliable way to keep track of work done, while also acting as a backup.

## Maven
The Git repository is then built using Maven, and a .jar file is generated and stored.

## Docker
The previously generated .jar is used in a Docker image. This image is pushed to the Docker repository and deployed to the run-time machine using SSH.

## Jenkins
To coordinate every step of the Dev-Ops cycle, Jenkins is used, enabling automated builds, tests and potential deployments upon each commit. Jenkins pulls the latest version of the Git Repository and goes through the Stages mentioned above (building with maven, placing the .jar in a docker image and deploying remotely).
