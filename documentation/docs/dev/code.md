# Code guidelines
In this page, you can read an overview of the code that was written to develop this application. This includes an overview of the classes and their respective methods. 

To note that some of these methods depend on variables that can have their value defined on the application.properties file.

## Data types
To aid the modelling of this solution, some additional data types were defined as follows:
### GpsData
Data of a received GPS point.

**Attributes:**

* **latitude** - the latitude of the GPS coordinate
* **longitude** - the longitude of the GPS coordinate
* **username** - the user that generated this point
* **timestamp** - timestamp of the generation

Only includes getters and setters.

### PointOfInterest
Information about a single point of interest.

**Attributes:**

* **name** - name of the point of interest
* **centerLatitude** - the latitude of the center of this point of interest
* **centerLongitude** - the longitude of the center of this point of interest
* **range** - range around the center coordinate that this point of interest covers, in meters

Besides getters and setters, the following methods are available:

* **boolean** **distance**(**double** lat1, **double** lat2, **double** lon1, **double** lon2)<br>
Returns the distance between two GPS coordinates in meters.
* **boolean** **canSense**(**double** latitude, **double** longitude)<br>
Returns if a GPS coordinate is within range of this point of interest.

### PoIEntry
Information about a GPS point in range of a point of interest.

**Attributes:**

* **pointId** - id of the point of interest that was triggered
* **timestamp** - timestamp of the crossing
* **username** - user that crossed the point of interest

Only includes getters and setters.

## CRUD repositories
In order to more efficently store our data, the following CRUD repositories were created:

* GpsRepository - GpsData
* PoIRepository - PointOfInterest
* PoIEntryRepository - PoIEntry

## Kafka components
Some functionality depends on the interaction with the Kafka broker, as such, we have some components dedicated to aid that exchange of information and keeps track of the recently read data.

### KafkaGpsGatherer
This component is a Kafka consumer responsible for gathering the data published on the raw GPS topic and storing in the GPS database.

* **void listen**(**String** message)<br>
Listens to the assigned topic and stores the data in the database.
* **void cacheEvictor**()<br>
This method is a scheduled task that is triggered every five seconds to check whether or not it should evict some of the caches that are present on the API, based on recent inserted data.
* **void evictSingleCacheValue**(**String** cacheName, **String** cacheKey)<br>
Evicts a single cache key's entries.
* **void evictAllCacheValue**(**String** cacheName)<br>
Fully evicts the given cache.

### KafkaGpsScheduler
This component is a Kafka producer responsible for retrieving the data from the external server and publishing it on the appropriate Kafka topic.

* **void send**()<br>
This method is a scheduled task that every four seconds checks if the external server has new data available since the last timestamp that is stored in the repository; in case there is, the entries are published on the topic (there's a maximum number of entries that can be published at once).
* **String** **getDataSince**(**long** timestamp)<br>
Returns data from the external server since the given timestamp.

### KafkaPoI
This component acts as a Kafka consumer listening to the messages from the raw gps topic and checks whether the GPS coordinates are in the range of a point of interest, in which case it'll behave as a Kafka producer and put a message on the corresponding point of interest's topic.

* **void populate**()<br>
In case there aren't any points of interest in the database, some default ones are created.
* **void listen**(**String** message)<br>
Consume from the raw GPS topic and publish in the point of interest topic if the GPS coordinates are in range.

## Controllers
Our Spring application presents two controllers to provide information to the outside.

### GpsAPIController
This controller provides two REST APIs, one for the GPS data and another one for the points of interest.

**/api/raw**<br>
Provides the GPS data the system has gathered without further processing. Can optionally filter some information with one or more of the following parameters:

* **username** - filter by username
* **start** - filter by starting timestamp
* **end** - filter by ending timestamp

**/api/poi**<br>
Provides information about points of interest: which ones exist, how many visits they've had and similarly what points of interest have been visited by a single user. Also allows to add a new point of interest. If no parameters are provided, a list of the existent points of interest is presented. These are the existing parameters:

* **add** - if present, will add a point of interest with the given name and with center latitude, center longitude and range present on the parameters **lat**, **lon** and **range** respectively.

* **id** - filter by point of interest id, if provided without username, returns all visits to the point of interest with this id
* **username** - filter by username, if provided without id, returns all the points of interest this user has visited
* **start** - filter by starting timestamp
* **end** - filter by ending timestamp

### MapController
This controller is responsible for presenting the web interface of our application by resorting to Thymeleaf for the templates. The mappings are as follows:

* **/** - home page, introducing our application
* **/follow** - see all recorded coordinates in a map and filter the information
* **/poi** - see information related to the points of interest and add new ones