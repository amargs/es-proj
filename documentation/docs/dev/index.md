# Requirements

## Android App
The Android App was developed using Java. To use the existing source code, Android Studio >3.4.0 is required for a successful build.

## Cloud Server
The server was developed in C, and by default is listening in port 1935.

## Spring Boot App
The following requirements must be installed for a full run:

* Git
* Maven
* Docker
* Docker-Compose
* Java 8

