﻿##What is GPSTracker?
GPSTracker is a system that collects user’s smartphone GPS information over time and stores it in a Kafka Broker to be consumed by a Database. The system will retrieve this information from the database, process it and then expose it in a REST API. 

This interface will be used by several client UI’s to provide end users with useful data like timed trajectories, time leaderboards for each route, most popular routes, that can be integrated in any application’s use case.

##If I were to join the team, how could I easily jump in development?
We've documented some of the most important aspects of development in our [development](dev/) area

##Who's developing GPSTracker?
You can read more about the team and our progress on the [team section](team/about.md).
