## The team
||Name|Contact|
|:----:|:----|:----|
|![fotos](/assets/images/Team/marg.PNG)|Ana Margarida Silva|[margaridaocs@ua.pt](mailto:margaridaocs@ua.pt)|
|![fotos](/assets/images/Team/brunoFoto.jpg)|Bruno Ribeiro|[bruno.rs.ribeiro@ua.pt](mailto:bruno.rs.ribeiro@ua.pt)|
|![fotos](/assets/images/Team/hugo.PNG)|Hugo Sampaio|[hsampaio@ua.pt](mailto:hsampaio@ua.pt)|
|![fotos](/assets/images/Team/chaves.PNG)|Ricardo Chaves|[ricardochaves@ua.pt](mailto:ricardochaves@ua.pt)|
