## API formats

### Query full file
Gets full file with positions for MAC.

	https://es.ricardochaves.pt/MAC

### Query MAC timestamps
Gets positions of user with MAC starting on timestamp T

	https://es.ricardochaves.pt/api/MAC?s=T

### Query all MACs timestamps
Gets positions of all users starting on timestamp T. Ordered by timestamp.

	https://es.ricardochaves.pt/api?s=T
