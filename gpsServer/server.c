// original: https://github.com/pradyuman/socket-c

/* Generic */
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Network */
#include <netdb.h>
#include <sys/socket.h>

/*FILE SYSTEM*/
#include <dirent.h>
#include <limits.h>
#include <pthread.h>
#include <time.h>
#include <dirent.h>

/* OPEN */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUF_SIZE 1<<22
#define DESCRIPTION "DESCRIPTION: HTTP server receiving and storing GPS data of each user\n\n"

/* Function declarations */
struct addrinfo *getAddrInfo(char* port);
int bindListener(struct addrinfo *info);
void header(int handler, int status);
void httpReply(int handler, char* data);
char* getBodyFromPost(char* post);
char* getTime(char* s);
void resolve(int handler);
struct arg_struct {
	int arg1;
	int arg2;
};
void *threadFunc(void *arguments);
int main(int argc, char **argv);
int handleData(char* data);
int findSubstring(char* data, char* substring);
int findSubstringFirst(char* data, char* substring);
int findLastOccurence(char* string, char match);
int countAll(char* string, char* match);
void decodeData(char** data);
char* strcut(char* haystack, char* needle);
int indexOf(FILE *fptr, const char *word, int *line, int *col, int *charsToSkip);
int indexOfBigger(FILE *fptr, const char *word, int *charsToSkip);
char* getTimestampFromLine(char* str);
int compareTimestamp(const char* t1, const char* t2);

/* ------------- Main --------------- */
int main(int argc, char **argv) {
	printf("%s", DESCRIPTION);
	if (argc != 2) {
		fprintf(stderr, "USAGE: %s <port>\n",argv[0]);
		return 1;
	}

	fd_set readfs;
	FD_ZERO(&readfs);

	// bind a listener
	int server = bindListener(getAddrInfo(argv[1]));
	if (server < 0) {
		fprintf(stderr, "[main:72:bindListener] Failed to bind at port %s\n", argv[1]);
		return 2;
	}

	if (listen(server, 10) < 0) {
		perror("[main:82:listen]");
		return 3;
	}

	// accept incoming requests asynchronously
	int handler;
	socklen_t size;
	struct sockaddr_storage client;
	while (1) {
		size = sizeof(client);
		printf("[main] Accepting...\n");
		handler = accept(server, (struct sockaddr *)&client, &size);
		if (handler < 0) {
			perror("[main:82:accept]");
			continue;
		}
		printf("[main] Accepted!\n");

		// start thread to handle a request

		pthread_t tid;
		struct arg_struct args;
		args.arg1 = server;
		args.arg2 = handler;
		printf("[main] creating thread!\n");
		if(pthread_create(&tid, NULL, threadFunc,(void *)&args) != 0){
			perror("[main:pthread]");
			break;
		}
		pthread_detach(tid);
	}

	close(server);
	return 0;
}

/* ---------------------------------------------- */

void *threadFunc(void *arguments){
	struct arg_struct *args = arguments;
	int server = args->arg1;
	int handler = args->arg2;

	resolve(handler);
	close(handler);
}

// handles a request
void resolve(int handler) {
	int status = 0;
	char buf[BUF_SIZE];
	char *method;

	recv(handler, buf, BUF_SIZE, 0);	
	
	if(strlen(buf) <= 0){
		return;
	}

	method = strcut(buf, " ");
	char* timestamp = getTime("");
	printf("\n-------------------------------\n%s-------------------------------\n%s\n---------------\n",timestamp,buf);
	free(timestamp);

	if (strcmp(method, "POST") == 0){
		//// HANDLING POST REQUEST
		printf("[resolve] POST!\n");
		char *content = getBodyFromPost(buf); // content now has data sent in POST
		if (content == NULL){
			printf("[resolve] content is NULL: %s\n",content);
			return;
		}
		printf("[resolve] Decoding data...\n");
		decodeData(&content);
		printf("[resolve] Data decoded.\n");
		content[findLastOccurence(content,'}')+1] = '\0';

		int status = handleData(content);
		free(content);
		if(status==0){
			header(handler, 0);
		}else{
			header(handler,-1);
		}

	}
	else if (strcmp(method, "GET") == 0){
		// return
		printf("GET!\n");
		// /api/...
		char* url_tmp = strcut(buf, " HTTP");
		char *url = strstr(url_tmp,"/")+strlen("/");

		printf("[resolve] got: %s\n",url);

		if(strstr(url,":")!=NULL){
			printf("[resolve] Processing request for single mac...\n");
			char* startTime = strstr(url,"?s=")+strlen("?s=");
			printf("[resolve] startTime: %s\n",startTime);

			char *fName = (char*)calloc((strlen(url)+strlen("dataFiles/") + 1),sizeof(char));
			sprintf(fName,"dataFiles/");
			char* f_tmp = strcut(url,"?s=");
			char* f = f_tmp + strlen("api/");
			strcat(fName,f);

			printf("[resolve] fname: [%s]\n",fName);

			char *source = NULL;
			source = "Line asdasd\nioioio";
			FILE *fp = fopen(fName, "r");
			
			free(fName);
			free(f_tmp);

			if (fp != NULL) {
				// Go to the end of the file.
				if (fseek(fp, 0L, SEEK_END) == 0) {
					// Get the size of the file.
					long bufsize = ftell(fp);
					if (bufsize == -1) {
						header(handler,-1);
						return;
					}

					// Go back to the start of the file.
					if (fseek(fp, 0L, SEEK_SET) != 0) {
						header(handler,-1);
						return;
					}

					int line;
					int col;
					int charsToSkip;

					// Find index of word in fptr
					indexOf(fp, startTime, &line, &col, &charsToSkip);

				    source = calloc((bufsize + 1),sizeof(char));
				    if (line != -1){
				        printf("[resolve] '%s' found at line: %d, col: %d\n", startTime, line + 1, col + 1);
				        printf("[resolve] charsToSkip: %d\n",charsToSkip);
				        // Read file starting from the line
				        if(fseek(fp,charsToSkip,0)==0){
				        	size_t newLen = fread(source, sizeof(char), bufsize, fp);
					        if ( ferror( fp ) != 0 ) {
					            fputs("Error reading file", stderr);
					        } else {
					            source[newLen++] = '\0';
					        }
				        }
				    }
				    else{
				        printf("[resolve] '%s' does not exist.\n", startTime);
				    }
				}
				fclose(fp);

				printf("[resolve] Sending data back...\n");

				httpReply(handler,source);
				free(source);
			}else{
				header(handler,-1);
			}
		}else{
			printf("[resolve] Processing request to search all users...\n");
			char* startTime = strstr(url,"?s=")+strlen("?s=");
			printf("[resolve] startTime: [%s]\n",startTime);

			// Search all files in dataFiles/
			// return lines where timestamp>startTime
			// cat * | sort -k 1.37,1.49
			char* cmd = "cat dataFiles/* | sort -u -k 1.37,1.49";
			char* source;
			int bufsize = 1<<20;
			size_t len = bufsize;
			FILE *fp = popen(cmd, "r");
			if (fp != NULL) {
				int charsToSkip;

				// Find index of word in fptr
				int idx_ret = indexOfBigger(fp, startTime, &charsToSkip);

			    source = calloc((bufsize + 1),sizeof(char));
			    if (idx_ret != -1){
			        printf("[resolve] charsToSkip: %d\n",charsToSkip);
			        
			        size_t newLen = fread(source, sizeof(char), bufsize, fp);
				    if ( ferror( fp ) != 0 ) {
				        fputs("Error reading file", stderr);
				    } else {
				        source[newLen++] = '\0'; /* Just to be safe. */
				    }
			    }
			    else{
			        printf("[resolve] '%s' does not exist.\n", startTime);
			    }
				pclose(fp);

				printf("[resolve] Sending data back...\n");
				//send(handler, source, strlen(source), 0);
				//printf("[resolve] data: %s\n",source);
				httpReply(handler,source);
				free(source);
			}else{
				header(handler,-1);
			}
		}
		free(url_tmp);
	}
	free(method);
	
	return;
}

// Saves the received data in the user's file
int handleData(char* data){
	//data:: {"addr":"80:80:80:80:80:80","time":"15524242424","lat":"40,0000000","lon":"-8,877770","accuracy":"2000,00000"}
	int i;
	if((i=findSubstring(data,"\"addr\":"))<0) return -1;
	char mac[18];
	memcpy(mac,&data[i+1],17);
	mac[17]='\0';
	printf("[handleData] --mac:%s--\n",mac);

	char fileName[100];
	strcpy(fileName,"dataFiles/");
	strcat(fileName,mac);

	FILE *f;
	f = fopen(fileName,"a+");
	printf("[handleData] Writing: %s\n",data);
	fprintf(f, "%s\n", data);
	fclose(f);
	return 0;
}

/* ------------- Network Functions --------------- */

// Get addr information (used to bindListener)
struct addrinfo *getAddrInfo(char* port) {
	int r;
	struct addrinfo hints, *getaddrinfo_res;
	// Setup hints
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_socktype = SOCK_STREAM;
	if ((r = getaddrinfo(NULL, port, &hints, &getaddrinfo_res))) {
		fprintf(stderr, "[getAddrInfo:21:getaddrinfo] %s\n", gai_strerror(r));
		return NULL;
	}

	return getaddrinfo_res;
}

// Bind Listener
int bindListener(struct addrinfo *info) {
	if (info == NULL) return -1;

	int serverfd;
	for (;info != NULL; info = info->ai_next) {
		if ((serverfd = socket( info->ai_family,
								info->ai_socktype,
								info->ai_protocol)) < 0) {
			perror("[bindListener:35:socket]");
			continue;
		}

		int opt = 1;
		if (setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR,
			&opt, sizeof(int)) < 0) {
			perror("[bindListener:43:setsockopt]");
			return -1;
		}

		if (bind(serverfd, info->ai_addr, info->ai_addrlen) < 0) {
			close(serverfd);
			perror("[bindListener:49:bind]");
			continue;
		}

		freeaddrinfo(info);
		return serverfd;
	}

	freeaddrinfo(info);
	return -1;
}

/* ----------------------------------------------- */

/* ------------- HTTP functions --------------- */

// sends back request status
void header(int handler, int status) {
	char header[1000] = {0};
	if (status == 0) {
		sprintf(header, "HTTP/1.1 200 OK\nContent-Type: text/strings\nConnection: Closed\n\r\n\r\n");
	} else if (status == 1) {
		sprintf(header, "HTTP/1.1 403 Forbidden\r\n\r\n");
	} else {
		sprintf(header, "HTTP/1.1 404 Not Found\r\n\r\n");
	}

	printf("[header] Sending reply with: [%s]\n",header);
	send(handler, header, strlen(header), 0);
}

// HTTP response to GET request
void httpReply(int handler, char* data){
	char *headerTemplate = "HTTP/1.1 200 OK\nContent-Length: %d\nConnection: close\nContent-Type: text/plain; charset=utf-8\n\r\n";
	char *h = (char*)calloc((strlen(headerTemplate)+30),sizeof(char));
	sprintf(h,headerTemplate,strlen(data));
	char *reply = (char*)calloc((strlen(h)+strlen(data)+2),sizeof(char));
	strcpy(reply, h); 	// strlen(h)
	strcat(reply,data);	// strlen(data)
	strcat(reply,"\n"); // 2
	printf("[httpReply] Sending %d bytes...\n",(int)strlen(reply));
	send(handler, reply, strlen(reply), 0);

	free(reply);
	free(h);
}

// process an arriving POST to return only the body of the POST
// Caller must free return value.
char* getBodyFromPost(char* post){
	char c;
	char *ret;

	for(int i = 0; i < strlen(post); i++){
		c = post[i];
		if(i>3 && post[i-3]=='d' && post[i-2]=='a' && post[i-1]=='t' && post[i]=='a'){
			ret = (char*)calloc((strlen(post)-i-1),sizeof(char));
			memcpy(ret, &post[i+2], strlen(post)-i-1);
			return ret;
		}
	}

	return NULL;
}

/* ---------------------------------------------------- */

/* --------------------- Auxiliary functions ------------------- */

// adds timestamp to argument
char* getTime(char* s){
	time_t t = time(NULL);
	struct tm lt = *localtime(&t);

	char *buf;
	size_t sz;
	sz = snprintf(NULL, 0, "%d-%d-%d__%d:%d:%d %s\n",lt.tm_year + 1900, lt.tm_mon + 1, lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec, s); //get size needed
	buf = (char *)calloc((sz + 1),sizeof(char)); /* make sure you check for != NULL in real code */
	snprintf(buf, sz+1, "%d-%d-%d__%d:%d:%d %s\n",lt.tm_year + 1900, lt.tm_mon + 1, lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec, s);

	return buf;
}

// return index of char after substring, or -1 if not found
int findSubstring(char* data, char* substring){
	int stringIdx = 0;
	for(int i = 0; i < strlen(data); i++){
		if(data[i] == substring[stringIdx])stringIdx++;
		else stringIdx = 0;

		if(stringIdx == strlen(substring)){
			return i+1;
		}
	}
	return -1;
}

// Return index of first char of substring, or -1 if not found
int findSubstringFirst(char* data, char* substring){
	int stringIdx = 0;
	int ret = -1;
	for(int i = 0; i < strlen(data); i++){
		if(data[i] == substring[stringIdx]){
			stringIdx++;
			if(ret==-1) ret = i;
		}
		else{
			ret = -1;
			stringIdx = 0;	
		}

		if(stringIdx == strlen(substring)){
			return ret;
		}
	}
	return -1;
}

// Returns a new string of haystack up untill needle. If needle isn't found, returns NULL.
// Caller must free the returned value.
char* strcut(char* haystack, char* needle){
	int idx = findSubstringFirst(haystack, needle);
	if(idx == -1){
		return NULL;
	}else{
		char* ret;
		ret = (char*)calloc((idx+1),sizeof(char));
		memcpy(ret,haystack,idx);
		//printf("[strcut]ret: [%s]\n",ret);
		return ret;
	}
}

// data comes in the URL encoding format: (not necessarily a single line)
// %7Baddr%3A%2280%3A13%3A82%3A14%3AC0%3AA8%22%2Ctime%3A%221554574561367%22%2Clat%3A%2240%2C330094%22%2Clon%3A%22-8%2C839204%22%2Caccuracy%3A%222000%2C000000%22%7D
void decodeData(char** data){
	int count = countAll(*data,"%7D");
	printf("[decodeData] count: %d\n",count);
	char* decoded = (char*)calloc((strlen(*data)+count+1),sizeof(char));
	int x = 0;
	for(int i = 0; i < strlen(*data); i++){
		char c = (*data)[i];
		if(c=='%'){
			char tmp[3];tmp[0]=(*data)[i+1];tmp[1]=(*data)[i+2];tmp[2]='\0';
			char n = strtol(tmp,NULL,16);
			decoded[x++] = n;
			i+=2;
			continue;
		}
		decoded[x++] = c;
		if(c=='}'){
			decoded[x++] = '\r';
		}
	}
	free(*data);
	*data=decoded;
}

// Go through string and return the index of the last matching char
int findLastOccurence(char* string, char match){
	int idx = -1;
	for(int i = 0; i < strlen(string); i++){
		if(string[i]==match){
			idx = i;
		}
	}
	return idx;
}

// Returns the number of times 'match' appears in 'string'
int countAll(char* string, char* match){
	int count = 0;
	int stringIdx = 0;
	for(int i=0; i < strlen(string); i++){
		if(string[i] == match[stringIdx])stringIdx++;
		else stringIdx = 0;

		if(stringIdx == strlen(match)){
			count++;
		}
	}
	return count;
}

/**
 * Finds, first index of a word in given file. First index is represented
 * using line and column.
 */
int indexOf(FILE *fptr, const char *word, int *line, int *col, int *charsToSkip)
{	
	char str[1024];
	char *pos;

	*line = -1;
	*col  = -1;
	*charsToSkip = -1;

	int tmp = 0;

	while ((fgets(str, 1024, fptr)) != NULL){
		*line += 1;
		// Find first occurrence of word in str
		pos = strstr(str, word);

		if (pos != NULL){
			// First index of word in str is 
			// Memory address of pos - memory
			// address of str.
			*col = (pos - str);
			break;
		}else{
			tmp+=strlen(str);
		}
	}

	*charsToSkip = tmp;

	// If word is not found then set line to -1
	if (*col == -1)
		*line = -1;

	return *col;
}

/**
 *	Finds the first line where timestamp >= word
 */
int indexOfBigger(FILE *fptr, const char *word, int *charsToSkip)
{	
	char str[1024];

	*charsToSkip = -1;

	int tmp = 0;
	int ret = -1;
	char* t1;
	int r;

	while ((fgets(str, 1024, fptr)) != NULL){
		t1 = getTimestampFromLine(str);
		r = compareTimestamp(t1,word);
		free(t1);

		if (r > 0){
			// Found timestamp larger than required
			ret = 0;
			break;
		}else{
			tmp+=strlen(str);
		}
	}

	*charsToSkip = tmp;

	return ret;
}

char* getTimestampFromLine(char* str){
	char* sep = "\"time\":\"";
	char* line = strstr(str,sep)+strlen(sep);
	line = strcut(line,"\"");
	return line;
}

/**
 * Compare timestamps. Returns:
 * -1 if t1 < t2
 *  0 if t1 == t2
 *  1 if t1 > t2
 */
int compareTimestamp(const char* t1, const char* t2){
	for(int i = 0; i < strlen(t1); i++){
		char c1 = t1[i];
		char c2 = t2[i];
		if(c1>c2){
			return 1;
		}else if(c1<c2){
			return -1;
		}
	}
	return 0;
}