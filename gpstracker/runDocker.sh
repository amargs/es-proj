docker kill p3g1Latest || echo "Container not running"
docker container rm p3g1Latest || echo "Container not found"
docker image rm 192.168.160.86:5000/p3g1/gpstracker || echo "Docker image not found"
docker run --name p3g1Latest -d -p31000:31000 192.168.160.86:5000/p3g1/gpstracker