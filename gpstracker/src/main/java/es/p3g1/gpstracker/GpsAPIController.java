package es.p3g1.gpstracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;

/**
 * Provides an API that gives GPS data retrieved from kafka.
 * 
 * @param username the MAC usernameess of the target users
 * @param start    starting time
 * @param end      ending time
 * @param model    model
 * @return json response
 * @throws IOException
 */
@RestController
public class GpsAPIController {
    final static Logger loggger = (Logger) LoggerFactory.getLogger(GpsAPIController.class);

    @Autowired
    private GpsRepository gpsRep;
    @Autowired
    private PoIRepository pRep;
    @Autowired
    private PoIEntryRepository pEntriesRep;
    @Autowired
    private MyLogger logger;

    public GpsAPIController() {
    }

    // TODO split requests
    @Cacheable(value = "gps") // key="#username")
    @GetMapping("/api/raw")
    public String gpsAPI(@RequestParam(value = "username", required = false, defaultValue = "") String username,
            @RequestParam(value = "start", required = false, defaultValue = "") String start,
            @RequestParam(value = "end", required = false, defaultValue = "") String end, Model model)
            throws IOException {

        logger.info("Fetched data for raw GPS API.", loggger);
        List<GpsData> gpsData;

        if (username.equals("")) {
            if (start.equals("")) {
                if (end.equals(""))
                    gpsData = (List<GpsData>) gpsRep.findAll();
                else
                    gpsData = gpsRep.findByTimestampBefore(new Timestamp(Long.parseLong(end)));
            } else {
                if (end.equals(""))
                    gpsData = gpsRep.findByTimestampAfter(new Timestamp(Long.parseLong(start)));
                else {
                    gpsData = gpsRep.findByTimestampBeforeAndTimestampAfter(new Timestamp(Long.parseLong(end)),
                            new Timestamp(Long.parseLong(start)));
                }
            }
        } else {
            if (start.equals("")) {
                if (end.equals(""))
                    gpsData = (List<GpsData>) gpsRep.findByUsername(username);
                else
                    gpsData = gpsRep.findByUsernameAndTimestampBefore(username, new Timestamp(Long.parseLong(end)));
            } else {
                if (end.equals(""))
                    gpsData = gpsRep.findByUsernameAndTimestampAfter(username, new Timestamp(Long.parseLong(start)));
                else {
                    gpsData = gpsRep.findByUsernameAndTimestampBeforeAndTimestampAfter(username,
                            new Timestamp(Long.parseLong(end)), new Timestamp(Long.parseLong(start)));
                }
            }
        }

        String response = "[";
        ObjectMapper mapper = new ObjectMapper();
        for (GpsData gps : gpsData) {
            String json = mapper.writeValueAsString(gps);
            response += json + ",";
        }

        if (response.equals("["))
            return "[]";
        return response.substring(0, response.length() - 1) + "]";
    }

    @GetMapping("/api/poi")
    public String poiAPI(@RequestParam(value = "add", defaultValue = "") String poiName,
            @RequestParam(value = "lat", required = false) Double lat,
            @RequestParam(value = "lon", required = false) Double lon,
            @RequestParam(value = "range", required = false) Double range,
            @RequestParam(value = "id", defaultValue = "-1") long poiID,
            @RequestParam(value = "username", defaultValue = "") String username,
            @RequestParam(value = "start", defaultValue = "") String start,
            @RequestParam(value = "end", defaultValue = "") String end) throws IOException {

        if (!poiName.equals("")) {
            try {
                if (pRep.findByName(poiName) != null) {
                    logger.info("Attempt to insert already existing point", loggger);
                    return "{\"error\":true, \"exists\": true}";
                }
                PointOfInterest p = new PointOfInterest(poiName, lat, lon, range);
                pRep.save(p);
                logger.info("Added point of interest: " + poiName, loggger);
            } catch (Exception e) {
                logger.info("Invalid attempt to insert point of interest.", loggger);
                return "{\"error\":true,\"exists\": false}";
            }
        }
        String response = "[";
        logger.info("Fetched data for point of interest API.", loggger);

        if (poiID == -1) {
            // no arguments will return all points of interest
            if (username.equals("")) {
                for (PointOfInterest p : pRep.findAll())
                    response += "{\"id\": " + p.getId() + ", \"name\": \"" + p.getName() + "\", \"centerLat\": "
                            + p.getCenterLatitude() + ", \"centerLon\": " + p.getCenterLongitude() + ", \"range\": " + p.getRange() + "},";
            } else if (start.equals("")) {
                // username only will return all visited points and date
                if (end.equals(""))
                    response += getUserVisits(pEntriesRep.findByUsernameOrderByTimestampAsc(username));
                else
                    response += getUserVisits(
                            pEntriesRep.findByUsernameAndTimestampAfter(username, new Timestamp(Long.parseLong(end))));
            } else {
                if (end.equals(""))
                    response += getUserVisits(pEntriesRep.findByUsernameAndTimestampBefore(username,
                            new Timestamp(Long.parseLong(start))));
                else
                    response += getUserVisits(pEntriesRep.findByUsernameAndTimestampBeforeAndTimestampAfter(username,
                            new Timestamp(Long.parseLong(start)), new Timestamp(Long.parseLong(end))));
            }
        } else {
            // if PoI is provided but no username, it will return the amount of times it was
            // visited by each user and total times
            if (username.equals("")) {
                if (start.equals("")) {
                    if (end.equals(""))
                        response += getPoIVisits(poiID, pEntriesRep.findByPointId(poiID));
                    else
                        response += getPoIVisits(poiID,
                                pEntriesRep.findByPointIdAndTimestampAfter(poiID, new Timestamp(Long.parseLong(end))));
                } else {
                    if (end.equals(""))
                        response += getPoIVisits(poiID, pEntriesRep.findByPointIdAndTimestampBefore(poiID,
                                new Timestamp(Long.parseLong(start))));
                    else
                        response += getPoIVisits(poiID, pEntriesRep.findByPointIdAndTimestampBeforeAndTimestampAfter(
                                poiID, new Timestamp(Long.parseLong(start)), new Timestamp(Long.parseLong(end))));
                }
            } else {
                // if both PoI and username are provided, returns vists to that point by the
                // user
                if (start.equals("")) {
                    if (end.equals(""))
                        response += getUserVisitPoI(pEntriesRep.findByPointIdAndUsername(poiID, username));
                    else
                        response += getUserVisitPoI(pEntriesRep.findByPointIdAndUsernameAndTimestampAfter(poiID,
                                username, new Timestamp(Long.parseLong(end))));
                } else {
                    if (end.equals(""))
                        response += getUserVisitPoI(pEntriesRep.findByPointIdAndUsernameAndTimestampBefore(poiID,
                                username, new Timestamp(Long.parseLong(start))));
                    else
                        response += getUserVisitPoI(
                                pEntriesRep.findByPointIdAndUsernameAndTimestampBeforeAndTimestampAfter(poiID, username,
                                        new Timestamp(Long.parseLong(start)), new Timestamp(Long.parseLong(end))));
                }
            }
        }

        if (response.equals("["))
            return "[]";
        return response.substring(0, response.length() - 1) + "]";
    }

    private String getUserVisits(List<PoIEntry> entries) {
        String response = "";
        long prevId = -1;
        Timestamp arrived = new Timestamp(1546300800), left = new Timestamp(1546300800);
        for (PoIEntry p : entries) {
            if (p.getPointId() == prevId) {
                // if the two points are less than half an hour apart, user didn't leave
                if (p.getTimestamp().getTime() - left.getTime() <= 30 * 60 * 1000)
                    left = p.getTimestamp();
                else {
                    response += "{\"pointId\": " + p.getPointId() + ", \"arrived\": \"" + arrived.getTime()
                            + "\", \"left\": \"" + left.getTime() + "\"},";
                    arrived = p.getTimestamp();
                    left = p.getTimestamp();
                }
            } else {
                if (prevId > -1)
                    response += "{\"pointId\": " + prevId + ", \"arrived\": \"" + arrived.getTime() + "\", \"left\": \""
                            + left.getTime() + "\"},";
                prevId = p.getPointId();
                arrived = p.getTimestamp();
                left = p.getTimestamp();
            }
        }
        if (prevId > -1)
            response += "{\"pointId\": " + prevId + ", \"arrived\": \"" + arrived.getTime() + "\", \"left\": \""
                    + left.getTime() + "\"},";
        return response;
    }

    // TODO account for visit duration
    private String getPoIVisits(Long id, List<PoIEntry> entries) {
        Map<String, Integer> visitorCount = new HashMap<>();
        for (PoIEntry p : entries) {
            if (visitorCount.containsKey(p.getUsername()))
                visitorCount.put(p.getUsername(), visitorCount.get(p.getUsername()) + 1);
            else
                visitorCount.put(p.getUsername(), 1);
        }

        String response = "";
        // FIXME
        try {
            PointOfInterest p = pRep.findById(id).get();
            if (p != null)
                response += "{\"name\": \"" + p.getName() + "\",";
        } catch (Exception e) {
        }
        int total = 0;
        String lines = " \"visitors\": [";
        for (Map.Entry<String, Integer> entry : visitorCount.entrySet()) {
            total += entry.getValue();
            lines += "{\"username\": \"" + entry.getKey() + "\", \"count\": " + entry.getValue() + "},";
        }
        response += " \"total\": " + total + ",";
        if (total == 0)
            return response + lines + "]},";
        return response + lines.substring(0, lines.length()-1) + "]},";
    }

    private String getUserVisitPoI(List<PoIEntry> entries) {
        String response = "";
        boolean firstEntry = true;
        Timestamp arrived = new Timestamp(1546300800), left = new Timestamp(1546300800);
        for (PoIEntry p : entries) {
            if (firstEntry) {
                firstEntry = false;
                arrived = p.getTimestamp();
                left = p.getTimestamp();
            } else {
                // if the two points are less than half an hour apart, user didn't leave
                if (p.getTimestamp().getTime() - left.getTime() <= 30 * 60 * 1000)
                    left = p.getTimestamp();
                else {
                    response += "{\"arrived\": \"" + arrived.getTime() + "\", \"left\": \"" + left.getTime() + "\"},";
                    arrived = p.getTimestamp();
                    left = p.getTimestamp();
                }
            }
        }
        if (!firstEntry)
            response += "{\"arrived\": \"" + arrived.getTime() + "\", \"left\": \"" + left.getTime() + "\"},";
        return response;
    }
}
