package es.p3g1.gpstracker;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.sql.Timestamp;

@Entity
public class GpsData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double latitude;
    private double longitude;
    private String username;
    private Timestamp timestamp;

    protected GpsData() {
    }

    public GpsData(double latitude, double longitude, String username, Timestamp timestamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.username = username;
        this.timestamp = timestamp;
    }

    public GpsData(double latitude, double longitude, String username, long timestamp) {
        this(latitude, longitude, username, new Timestamp(timestamp));
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getUsername() {
        return username;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "{" + " id='" + id + "'" + ", latitude='" + latitude + "'" + ", longitude='" + longitude + "'"
                + ", username='" + username + "'" + ", timestamp='" + timestamp + "'" + "}";
    }

}