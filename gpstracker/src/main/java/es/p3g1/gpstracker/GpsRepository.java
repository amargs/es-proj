package es.p3g1.gpstracker;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

//TODO get a better database
public interface GpsRepository extends CrudRepository<GpsData, Long> {
    List<GpsData> findByUsername(String username);

    List<GpsData> findByTimestampAfter(Timestamp timestamp);

    List<GpsData> findByTimestampBefore(Timestamp timestamp);

    List<GpsData> findByTimestampBeforeAndTimestampAfter(Timestamp before, Timestamp after);

    List<GpsData> findByUsernameAndTimestampAfter(String username, Timestamp timestamp);

    List<GpsData> findByUsernameAndTimestampBefore(String username, Timestamp timestamp);

    List<GpsData> findByUsernameAndTimestampBeforeAndTimestampAfter(String username, Timestamp before, Timestamp after);

    GpsData findFirst1ByOrderByTimestampDesc();

    @Query("SELECT DISTINCT username FROM GpsData")
    List<String> findAllUsernames();
}
