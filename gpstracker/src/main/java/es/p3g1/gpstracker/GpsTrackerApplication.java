package es.p3g1.gpstracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableCaching
public class GpsTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GpsTrackerApplication.class, args);
	}

}
