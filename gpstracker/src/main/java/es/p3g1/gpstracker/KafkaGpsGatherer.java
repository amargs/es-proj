package es.p3g1.gpstracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class KafkaGpsGatherer {
    final static Logger loggger = (Logger) LoggerFactory.getLogger(KafkaGpsGatherer.class);

    @Autowired
    private GpsRepository gpsRep;
    @Autowired
    CacheManager cacheManager;
    private final RandomUsernameGenerator r;
    private Map<String, Boolean> evictor;
    @Autowired
    private MyLogger logger;

    public KafkaGpsGatherer(RandomUsernameGenerator r) {
        this.r = r;
        evictor = new HashMap<>();
    }

    @KafkaListener(topics = "${es.kafka.topicname.rawgps}")
    public void listen(String message) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jn;
        GpsData gpsData;
        try {
            jn = mapper.readTree(message);
            gpsData = new GpsData(Double.parseDouble(jn.get("lat").asText().replace(",", ".")),
                    Double.parseDouble(jn.get("lon").asText().replace(",", ".")),
                    r.generateUsername(jn.get("addr").asText()), Long.parseLong(jn.get("time").asText()));
            gpsRep.save(gpsData);
            evictor.put(gpsData.getUsername(), true);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Scheduled(fixedDelay = 5000)
    private void cacheEvictor() {
        for (Map.Entry<String, Boolean> entry : evictor.entrySet()) {
            if (entry.getValue()) {
                logger.info("Triggered cache eviction for username " + entry.getKey(), loggger);
                evictAllCacheValues("gps");
                // evictSingleCacheValue("gps", entry.getKey());
                evictor.put(entry.getKey(), false);
            }
        }
    }

    private void evictSingleCacheValue(String cacheName, String cacheKey) {
        cacheManager.getCache(cacheName).evict(cacheKey);
    }

    public void evictAllCacheValues(String cacheName) {
        cacheManager.getCache(cacheName).clear();
    }
}