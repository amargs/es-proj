package es.p3g1.gpstracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class KafkaGpsScheduler {
    final static Logger loggger = (Logger) LoggerFactory.getLogger(KafkaGpsScheduler.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private GpsRepository gpsRep;
    @Value("${es.kafka.topicname.rawgps}")
    private String gpsTopic;
    @Value("${es.server.url}")
    private String serverURL;
    @Value("${es.maxentries}")
    private int maxEntries;
    @Autowired
    private MyLogger logger;

    public KafkaGpsScheduler() {
    }

    @Scheduled(fixedDelay = 4000)
    public void send() {
        GpsData gpsData = gpsRep.findFirst1ByOrderByTimestampDesc();
        String data;

        long t;
        if (gpsData == null)
            t = 1546300800;
        else
            t = gpsData.getTimestamp().getTime();
        data = getDataSince(t);
        
        if (data != null) {
            String[] parsed = data.split("\n");
            logger.info("Retrieved GPS data from external server since timestamp: " + t, loggger);
            int entries = 0;
            for (String entry : parsed) {
                if (entries >= maxEntries)
                    break;
                kafkaTemplate.send(gpsTopic, entry);
                entries++;
            }
        }
    }

    private String getDataSince(long timestamp) {
        String resourceUrl = serverURL + "api?s=" + timestamp;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);

        return response.getBody();
    }

}