package es.p3g1.gpstracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class KafkaPoI {
    final static Logger loggger = (Logger) LoggerFactory.getLogger(KafkaPoI.class);

    @Autowired
    private PoIRepository pRep;
    @Autowired
    private PoIEntryRepository pEntriesRep;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Value("${es.kafka.topicname.poi-basename}")
    private String topicBasename;
    private final RandomUsernameGenerator r;
    
    public KafkaPoI(RandomUsernameGenerator r) {
        this.r = r;
    }
    
    @PostConstruct
    private void populate() {
        List<PointOfInterest> points = pRep.findAll();
        if (points.size() == 0) {
            points = new ArrayList<>();
            points.add(new PointOfInterest("DETI", 40.633202, -8.659468, 100));
            points.add(new PointOfInterest("Glicinias", 40.626948, -8.644233, 125));
            points.add(new PointOfInterest("Ramona", 40.638130, -8.651356, 20));
            points.add(new PointOfInterest("Parque Infante D. Pedro", 40.635969, -8.653471, 110));
            points.add(new PointOfInterest("Ria de Aveiro", 40.640661, -8.657249, 20));
            points.add(new PointOfInterest("Praça do Peixe", 40.642381, -8.655165, 150));
            points.add(new PointOfInterest("Igreja Matriz de Esgueira", 40.647745, -8.627141, 20));
    
            for (PointOfInterest p : points)
                pRep.save(p);
        }
    }

    // TODO check before adding to topic
    @KafkaListener(topics = "${es.kafka.topicname.rawgps}", groupId = "${es.kafka.poi-group-id}")
    public void listen(String message) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jn;
        try {
            jn = mapper.readTree(message);
            double latitude = Double.parseDouble(jn.get("lat").asText().replace(",", "."));
            double longitude = Double.parseDouble(jn.get("lon").asText().replace(",", "."));
            String username = r.generateUsername(jn.get("addr").asText());
            Timestamp timestamp = new Timestamp(Long.parseLong(jn.get("time").asText()));

            List<PointOfInterest> points = pRep.findAll();
            for (PointOfInterest poi : points) {
                if (poi.canSense(latitude, longitude)) {
                    PoIEntry entry = new PoIEntry(poi.getId(), timestamp, username);
                    pEntriesRep.save(entry);
                    String msg = "[" + timestamp.toLocaleString() + "] " + poi.getName() + ": " + username
                            + " is in range.";
                    kafkaTemplate.send(topicBasename + poi.getId(), msg);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}