package es.p3g1.gpstracker;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

// TODO make this pretty
@Controller
public class MapController {
	@Value("${es.machineip}")
	private String machineIP;
	@Value("${server.port}")
	private String serverPort;
	@Autowired
	private GpsRepository gpsRep;
	@Autowired
	private PoIRepository pRep;

	@GetMapping("/")
	public String mapIndex(Model model) {
		model.addAttribute("apiURL", machineIP + ":" + serverPort + "/api");
		model.addAttribute("usernames", gpsRep.findAllUsernames());
		return "index";
	}

	@GetMapping("/follow")
	public String mapFollow(Model model) {
		model.addAttribute("apiURL", machineIP + ":" + serverPort + "/api");
		model.addAttribute("usernames", gpsRep.findAllUsernames());
		return "follow";
	}

	@GetMapping("/poi")
	public String poiPage(Model model) {
		model.addAttribute("apiURL", machineIP + ":" + serverPort + "/api");
		model.addAttribute("usernames", gpsRep.findAllUsernames());
		model.addAttribute("points", pRep.findAll());
		return "poi";
	}
}
