package es.p3g1.gpstracker;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@Component
public class MyLogger {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Value("${es.kafka.topicname.logging}")
    private String topic;

    public MyLogger() {
    }


    public void info(String message, Logger logger) {
        String msg = new SimpleDateFormat("hh:mm:ss.SSS").format(new Date()) + " INFO - " + message; 
        logger.info(message);
        kafkaTemplate.send(topic, msg);
    }
}