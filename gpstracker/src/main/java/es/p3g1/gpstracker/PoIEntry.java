package es.p3g1.gpstracker;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PoIEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long pointId;
    private Timestamp timestamp;
    private String username;

    public PoIEntry(Long pointId, Timestamp timestamp, String username) {
        this.pointId = pointId;
        this.timestamp = timestamp;
        this.username = username;
    }

    protected PoIEntry() {
    }

    public Long getPointId() {
        return this.pointId;
    }

    public void setPointId(Long pointId) {
        this.pointId = pointId;
    }

    public Timestamp getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}