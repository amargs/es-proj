package es.p3g1.gpstracker;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface PoIEntryRepository extends CrudRepository<PoIEntry, Long> {
    List<PoIEntry> findByPointId(long pointId);

    List<PoIEntry> findByPointIdAndTimestampAfter(long pointId, Timestamp timestamp);

    List<PoIEntry> findByPointIdAndTimestampBefore(long pointId, Timestamp timestamp);

    List<PoIEntry> findByPointIdAndTimestampBeforeAndTimestampAfter(long pointId, Timestamp before, Timestamp after);

    List<PoIEntry> findByUsernameOrderByTimestampAsc(String username);

    List<PoIEntry> findByUsernameAndTimestampAfter(String username, Timestamp timestamp);

    List<PoIEntry> findByUsernameAndTimestampBefore(String username, Timestamp timestamp);

    List<PoIEntry> findByUsernameAndTimestampBeforeAndTimestampAfter(String username, Timestamp before,
            Timestamp after);

    List<PoIEntry> findByPointIdAndUsername(long pointId, String username);

    List<PoIEntry> findByPointIdAndUsernameAndTimestampAfter(long pointId, String username, Timestamp timestamp);

    List<PoIEntry> findByPointIdAndUsernameAndTimestampBefore(long pointId, String username, Timestamp timestamp);

    List<PoIEntry> findByPointIdAndUsernameAndTimestampBeforeAndTimestampAfter(long pointId, String username,
            Timestamp before, Timestamp after);
}
