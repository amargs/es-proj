package es.p3g1.gpstracker;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface PoIRepository extends CrudRepository<PointOfInterest, Long> {
    PointOfInterest findById(long id);

    PointOfInterest findByName(String Name);

    List<PointOfInterest> findAll();
}
